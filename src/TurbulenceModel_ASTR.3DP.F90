!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This module contains subroutines related turbulence models
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initial at 2016-02-311.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
Module TurbulenceModel
  !
  use CommVarDefine
  use CommFunction
  use ParallelProcess
  !
  implicit none
  !
  contains
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! This subroutine is to calculate eddy viscosity via standard 
  ! Spalart-Allmaras One-Equation Model
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !ref: http://turbmodels.larc.nasa.gov/spalart.html
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine SA_source
    !
    real(8) :: miu,nus,mu,omega,omega12,omega13,omega23
    real(8) :: s_c,var1,var2,d,nu_c,chi,fv1,fv2,ft2,r,g,fw
    real(8) :: source1,source2,sourcesum
    !
    do k=0,km
    do j=0,jm
    do i=0,im
    	!
    	miu=miucal(t(i,j,k))/ro(i,j,k)
    	nu_c=miut(i,j,k)
    	d=diswall(i,j,k)
    	!
      omega12=(du2(1,i,j,k)-du1(2,i,j,k))**2
      omega13=(du1(3,i,j,k)-du3(1,i,j,k))**2
      omega23=(du3(2,i,j,k)-du2(3,i,j,k))**2
      omega=sqrt(4.d0*(omega12+omega23+omega13))
      !
    	chi=nu_c/miu
    	fv1=chi**3/(chi**3+SA_cm1**3)
    	fv2=1.d0-(chi/(1.d0+chi*fv1))
    	!
      var1=nu_c*fv2/(vonkarman*d)**2
      s_c=omega+var1
      !
      ft2=SA_ct3*exp(-SA_ct4*chi**2)
      !
      var1=nu_c/(s_c*vonkarman**2*d**2)
      r=min(var1,10.d0)
      g=r+SA_cw2*(r**6-r)
      fw=g*((1.d0+SA_cw3**6)/(g**6+SA_cw3**6))**(1.d0/6.d0)
      !
      source1=SA_cb1*(1.d0-ft2)*s_c*nu_c
      source2=-(SA_cw1*fw-SA_cb1/vonkarman/vonkarman*ft2)*(nu_c/d)**2/Reynolds
      sourcesum=source1+source2
    	!
    	Residp1(i,j,k)=Residp1(i,j,k)+sourcesum*jacob(i,j,k)
    	!
    end do
    end do
    end do
    !
  end subroutine SA_source
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! The end of the subroutine SA_source.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! This subroutine is used to set the coefficient of the 
  ! Spalart-Allmaras model.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! 2010-08-04.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine SA_Coef
    !
    SA_cb1=0.1355d0
    SA_cb2=0.622d0
    SA_sig=0.66666666666667d0
    SA_cw1=SA_cb1/vonkarman/vonkarman+(1.d0+SA_cb2)/SA_sig
    SA_cw2=0.3d0
    SA_cw3=2.d0
    SA_cm1=7.1d0
    SA_ct1=1.d0
    SA_ct2=2.d0
    SA_ct3=1.2d0
    SA_ct4=0.5d0
    !
    prtur=0.9d0
    !
  end subroutine SA_Coef
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! End of the subroutine SA_Coef.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! This subroutine is used to calculte the convectional term of the
  ! transport equation of eddy viscosity, which is used in DES and RANS.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! Writen by Fang Jian, 2010-08-09.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine SA_ConvcCal
    !
    real(8) :: UU
    real(8), allocatable, dimension(:) :: Fcs
    real(8), allocatable, dimension(:) :: btemp,vtemp,wtemp
    !
    !!$ SAVE Fcs,btemp,vtemp,wtemp
    !!$OMP THREADPRIVATE(Fcs,btemp,vtemp,wtemp)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! btemp,vtemp,wtemp: temporary arraies
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    !!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,UU)
    !
    ! calculating along j direction
    allocate(Fcs(-4:im4))
    allocate(btemp(-4:im4),vtemp(0:im),wtemp(0:im))
    !   
    !!$OMP do
    do k=0,km
    do j=0,jm
      !
      do i=-4,im4
        UU=( ddi(1,i,j,k)*u1(i,j,k)+ddi(2,i,j,k)*u2(i,j,k)+            &
             ddi(3,i,j,k)*u3(i,j,k) )*jacob(i,j,k)
        Fcs(i)=UU*miut(i,j,k)
      end do
      !
      !calculating dro*miu*u*/di
      do i=-4,im4
        btemp(i)=Fcs(i)
      end do
      !
      call PTDS_RHS(btemp,vtemp,im,ndiff,npdci)
      call PTDS_Cal(Diafa_b0,Diafa_b1,Diafa_in,vtemp,wtemp,DCi,im,npdci)
      !
      do i=0,im
        Residp1(i,j,k)=Residp1(i,j,k)+wtemp(i)
      end do
    	!
    end do
    end do
    !!$OMP end do
    !
    deallocate(Fcs,btemp,vtemp,wtemp)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! end calculating along i direction
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! calculating along j direction
    allocate(Fcs(-4:jm4))
    allocate(btemp(-4:jm4),vtemp(0:jm),wtemp(0:jm))
    !
    !!$OMP do
    do k=0,km
    do i=0,im
      !
      do j=-4,jm4
        UU=( ddj(1,i,j,k)*u1(i,j,k)+ddj(2,i,j,k)*u2(i,j,k)+            &
             ddj(3,i,j,k)*u3(i,j,k) )*jacob(i,j,k)
        Fcs(j)=UU*miut(i,j,k)
      end do
      !
      !calculating the drou*/dj
      do j=-4,jm4
        btemp(j)=Fcs(j)
      end do
      !
      call PTDS_RHS(btemp,vtemp,jm,ndiff,npdcj)
      call PTDS_Cal(Djafa_b0,Djafa_b1,Djafa_in,vtemp,wtemp,DCj,jm,npdcj)
      !
      do j=0,jm
        Residp1(i,j,k)=Residp1(i,j,k)+wtemp(j)
      end do
      !
    end do
    end do
    !!$OMP end do
    deallocate(Fcs,btemp,vtemp,wtemp)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! end calculating along j direction
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    ! calculating along k direction
    if(.not. l2dcomp) then
      !
      allocate(Fcs(-4:km4))
      allocate(btemp(-4:km4),vtemp(0:km),wtemp(0:km))
      !
      !!$OMP do
      do j=0,jm
      do i=0,im
        !
        do k=-4,km4
          UU=( ddk(1,i,j,k)*u1(i,j,k)+ddk(2,i,j,k)*u2(i,j,k)+          &
               ddk(3,i,j,k)*u3(i,j,k) )*jacob(i,j,k)
          Fcs(k)=UU*miut(i,j,k)
        end do
        !
        !calculating the drou*)/dk
        do k=-4,km4
          btemp(k)=Fcs(k)
        end do
        call PTDS_RHS(btemp,vtemp,km,ndiff,npdck)
        call PTDS_Cal(Dkafa_b0,Dkafa_b1,Dkafa_in,vtemp,wtemp,DCk,km,   &
                                                                  npdck)
        do k=0,km
          Residp1(i,j,k)=Residp1(i,j,k)+wtemp(k)
        end do
        !
      end do
      end do
      !!$OMP end do
      deallocate(Fcs,btemp,vtemp,wtemp)
      !
    end if
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    ! end calculating along k direction
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !
    !!$OMP END PARALLEL
    !
  end subroutine SA_ConvcCal
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! The end of the subroutine SA_ConvcCal.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! This subroutine is used to calculate the diffusion and source terms 
  ! of the eddy viscous transport equation of Spalart-Allmaras model.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! 2010-08-05.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine SA_DiffuCal
    !
    real(8) :: miu,var1,var2
    !
    real(8), allocatable, dimension(:,:,:,:) :: dmut
    real(8), allocatable, dimension(:,:,:) :: temp1,temp2,temp3
    !
    real(8), allocatable, dimension(:) :: btemp,vtemp,wtemp
    !!$ SAVE btemp,vtemp,wtemp
    !!$OMP THREADPRIVATE(btemp,vtemp,wtemp)
    !
    allocate( dmut(1:3,-4:im4,-4:jm4,-4:km4)                           )
    allocate( temp1(-4:im4,-4:jm4,-4:km4),                             &
              temp2(-4:im4,-4:jm4,-4:km4),                             &
              temp3(-4:im4,-4:jm4,-4:km4)                           )
    !
    !!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,miu,chi,fv1,fv2,ft2,  &
    !!$OMP                  SS1,SS2,rt,gt,fw,sv1,sv2,sv3,sv4,var1,var2, &
    !!$OMP                           w11,w12,w13,w21,w22,w23,w31,w32,w33)
    !
    !!$OMP do
    do k=-2,km2
    do j=-2,jm2
    do i=-2,im2
      !
      dmut(1,i,j,k)=0.d0
      dmut(2,i,j,k)=0.d0
      dmut(3,i,j,k)=0.d0
      !
    end do
    end do
    end do
    !!$OMP end do
    !
    ! i direction derivative
    allocate(btemp(-4:im4),vtemp(0:im),wtemp(0:im))
    !
    !!$OMP do
    do k=0,km
    do j=0,jm
      !
      do i=-4,im4
        btemp(i)=miut(i,j,k)
      end do
      !
      call PTDS_RHS(btemp,vtemp,im,ndiff,npdci)
      call PTDS_Cal(Diafa_b0,Diafa_b1,Diafa_in,vtemp,wtemp,DCi,im,npdci)
      !
      do i=0,im
        dmut(1,i,j,k)=dmut(1,i,j,k)+wtemp(i)*ddi(1,i,j,k)
        dmut(2,i,j,k)=dmut(2,i,j,k)+wtemp(i)*ddi(2,i,j,k)
        dmut(3,i,j,k)=dmut(3,i,j,k)+wtemp(i)*ddi(3,i,j,k)
      end do
      !
    end do
    end do
    !!$OMP end do
    !
    deallocate(btemp,vtemp,wtemp)
    ! End of calculating i direction derivative.
    !
    ! j direction derivative
    allocate(btemp(-4:jm4),vtemp(0:jm),wtemp(0:jm))
    !
    !!$OMP do
    do k=0,km
    do i=0,im
      !
      do j=-4,jm4
        btemp(j)=miut(i,j,k)
      end do
      !
      call PTDS_RHS(btemp,vtemp,jm,ndiff,npdcj)
      call PTDS_Cal(Djafa_b0,Djafa_b1,Djafa_in,vtemp,wtemp,DCj,jm,npdcj)
      !
      do j=0,jm
        dmut(1,i,j,k)=dmut(1,i,j,k)+wtemp(j)*ddj(1,i,j,k)
        dmut(2,i,j,k)=dmut(2,i,j,k)+wtemp(j)*ddj(2,i,j,k)
        dmut(3,i,j,k)=dmut(3,i,j,k)+wtemp(j)*ddj(3,i,j,k)
      end do
      !
    end do
    end do
    !!$OMP end do
    !
    deallocate(btemp,vtemp,wtemp)
    ! End of calculating j direction derivative.
    !
    if(.not. l2dcomp) then
      ! k direction derivative
      allocate(btemp(-4:km4),vtemp(0:km),wtemp(0:km))
      !
      !!$OMP do
      do j=0,jm
      do i=0,im
        !
        do k=-4,km4
          btemp(k)=miut(i,j,k)
        end do
        call PTDS_RHS(btemp,vtemp,km,ndiff,npdck)
        call PTDS_Cal(Dkafa_b0,Dkafa_b1,Dkafa_in,vtemp,wtemp,DCk,km,   &
                                                                  npdck)
        do k=0,km
          dmut(1,i,j,k)=dmut(1,i,j,k)+wtemp(k)*ddk(1,i,j,k)
          dmut(2,i,j,k)=dmut(2,i,j,k)+wtemp(k)*ddk(2,i,j,k)
          dmut(3,i,j,k)=dmut(3,i,j,k)+wtemp(k)*ddk(3,i,j,k)
        end do
        !
      end do
      end do
      !!$OMP end do
      !
      deallocate(btemp,vtemp,wtemp)
      ! End of calculating k direction derivative.
    end if
    !
    !!$OMP do
    do k=0,km
    do j=0,jm
    do i=0,im
      miu=Miucal(t(i,j,k))/ro(i,j,k)
      !
      var1=jacob(i,j,k)*(miu+miut(i,j,k))/Reynolds/SA_sig
      !
      temp1(i,j,k)=( dmut(1,i,j,k)*ddi(1,i,j,k)+                       &
                     dmut(2,i,j,k)*ddi(2,i,j,k)+                       &
                     dmut(3,i,j,k)*ddi(3,i,j,k) )*var1
      temp2(i,j,k)=( dmut(1,i,j,k)*ddj(1,i,j,k)+                       &
                     dmut(2,i,j,k)*ddj(2,i,j,k)+                       &
                     dmut(3,i,j,k)*ddj(3,i,j,k) )*var1
      temp3(i,j,k)=( dmut(1,i,j,k)*ddk(1,i,j,k)+                       &
                     dmut(2,i,j,k)*ddk(2,i,j,k)+                       &
                     dmut(3,i,j,k)*ddk(3,i,j,k) )*var1
      !
      var1=jacob(i,j,k)*SA_cb2/Reynolds/SA_sig
      var2=var1*(dmut(1,i,j,k)**2+dmut(2,i,j,k)**2+dmut(3,i,j,k)**2)
      !
      Residp1(i,j,k)=Residp1(i,j,k)+var2
      !
    end do
    end do
    end do
    !!$OMP end do
    !
    call ArrySendRecv1(temp1,210)
    call ArrySendRecv1(temp2,211)
    call ArrySendRecv1(temp3,212)
    !
    ! i direction derivative
    allocate(btemp(-4:im4),vtemp(0:im),wtemp(0:im))
    !!$OMP do
    do k=0,km
    do j=0,jm
      !
      do i=-4,im4
        btemp(i)=temp1(i,j,k)
      end do
      !
      call PTDS_RHS(btemp,vtemp,im,ndiff,npdci)
      call PTDS_Cal(Diafa_b0,Diafa_b1,Diafa_in,vtemp,wtemp,DCi,im,npdci)
      !
      do i=0,im
        Residp1(i,j,k)=Residp1(i,j,k)+wtemp(i)
      end do
      !
    end do
    end do
    !!$OMP end do
    deallocate(btemp,vtemp,wtemp)
    ! End of calculating i direction derivative.
    !
    ! j direction derivative
    allocate(btemp(-4:jm4),vtemp(0:jm),wtemp(0:jm))
    !!$OMP do
    do k=0,km
    do i=0,im
      !
      do j=-4,jm4
        btemp(j)=temp2(i,j,k)
      end do
      !
      call PTDS_RHS(btemp,vtemp,jm,ndiff,npdcj)
      call PTDS_Cal(Djafa_b0,Djafa_b1,Djafa_in,vtemp,wtemp,DCj,jm,npdcj)
      !
      do j=0,jm
          Residp1(i,j,k)=Residp1(i,j,k)+wtemp(j)
      end do
      !
    end do
    end do
    !!$OMP end do
    deallocate(btemp,vtemp,wtemp)
    ! End of calculating j direction derivative.
    !
    ! k direction derivative
    if(.not. l2dcomp) then
      allocate(btemp(-4:km4),vtemp(0:km),wtemp(0:km))
      !!$OMP do
      do j=0,jm
      do i=0,im
        !
        do k=-4,km4
          btemp(k)=temp3(i,j,k)
        end do
        call PTDS_RHS(btemp,vtemp,km,ndiff,npdck)
        call PTDS_Cal(Dkafa_b0,Dkafa_b1,Dkafa_in,vtemp,wtemp,DCk,km,   &
                                                                  npdck)
        do k=0,km
          Residp1(i,j,k)=Residp1(i,j,k)+wtemp(k)
        end do
        !
      end do
      end do
      !!$OMP end do
      deallocate(btemp,vtemp,wtemp)
      ! End of calculating k direction derivative.
    end if
    !
    deallocate( dmut,temp1,temp2,temp3 )
    !
  end subroutine SA_DiffuCal
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  ! End of the subroutine SA_DiffuCal.
  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !!
End Module TurbulenceModel
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! The end of the module TurbulenceModel.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!