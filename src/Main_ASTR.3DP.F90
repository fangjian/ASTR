!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! This Program is the large eddy simulation using finit difference 
! method.
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initialized in 2008-09-18, BUAA, by Fang Jian 
! (fangjian@buaa.edu.cn) (fangjian19@gmail.com)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program main
  !
  use CommVarDefine
  use CommCal
  use ReadWrite
  use Geom
  use Initialize
  use Solver
  use DeltaTGen
  use Test
  use mpi
  use ParallelProcess
  use GridGen
  !!$ use OMP_LIB
  !
  implicit none
  !
  call MPIinitial
  !
  call Statement
  !
  call ReadInput
  !
  call SizeCal
  !
  call ParaPreProcess
  !
  call ParallelIni
  !
  call SubCommunitor
  !
  call RefConst
  !
  call outset
  !
  call AllocateCommVar
  !
#ifdef HDF5
  call H5ReadGrid
#else
  call ReadGrid
#endif
  !
  call GeomCal
  !
  call initial1
  !
  call RungeKutta
  !
  call MPI_FINALIZE(rc)
  !
  print*,' ** all the jobs are done!'
  !
end program main
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! End of the program main
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
